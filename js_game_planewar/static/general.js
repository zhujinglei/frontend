/*creating the map*/
let score = 0;
let total_live = 3;

//define the time including when to create the shooter
let time_creatShooter;
let time_ShooterMove;
let time_createEnemy;
let time_EnemyMove;

//define the shoot array; and enemy array
let shoot = [];
let em_array = [];
let map = document.getElementsByClassName('map')[0];

(function () { //(function(){})();this frunction can call himself function(){} this is the self-invoking function;
    let bgmap = document.getElementsByClassName('backgroundmap');
    //setting the picture's position (at the top)
    bgmap[0].style.top = "-730px";
    bgmap[1].style.top = "0px";
    //encapsulate the background picture let the background move instead of the plane
    function bgmove() {
        for (var i = 0; i < bgmap.length; i++) {
            var Top = parseInt(bgmap[i].style.top); //convert the string into integer
            Top++;
            if (Top >= 730) {
                Top = -730;
            }
            bgmap[i].style.top = Top + "px";
        } //for one movement the each background map move one px and every 0.03s move once
    }
    setInterval(bgmove, 30);
})();

/*creating attacting and bullets and encapuslate*/
/*create plane ad ecapuslate */
// let user;//instanlize the user

function User() {
    this.width = 50;
    this.height = 60;
    this.position = "absolute";
    this.userf = null; //null means this value exist but with an empty value; 
    this.src = './static/user.png';
    this.x;
    this.y;


    //to add a new element to the html DOM, you must create the element node first, and append it to an existing element
    this.createUser = function () {
        if (this.userf == null) {
            this.userf = document.createElement("img");
            this.userf.style.width = this.width + "px";
            this.userf.style.height = this.height + "px";
            this.userf.style.position = this.position;
            this.userf.style.zIndex = 1;
            this.userf.src = this.src;
            this.userf.style.left = "170px"; //400/2-60/2
            this.userf.style.top = "630px"; //630-70-30
            map.appendChild(this.userf);
        }
    };

    // UserMove is used to functionize the movement of the plane;
    this.UserMove = function (x, y) { //this wil link to the eventlisener from mouse
        this.x = x;
        this.y = y;
        this.userf.style.left = x + "px";
        this.userf.style.top = y + "px";
    }
}
//let shooter; //create the shoot action

function Shoot() {
    this.width = 10;
    this.height = 20;
    this.shooterf = null;
    this.position = "absolute";
    this.src = "./static/plane_attact.png";
    this.x;
    this.y;


    //create the shoots
    this.creatShooter = function (User) {
        if (this.shooterf == null) {
            this.shooterf = document.createElement("img");
            this.shooterf.style.width = this.width + "px";
            this.shooterf.style.height = this.height + "px";
            this.shooterf.style.position = this.position;
            this.shooterf.style.zIndex = 1;
            this.shooterf.src = this.src;
            map.appendChild(this.shooterf); //add the element into the map;

            //cordination the shoot x= the left of the plane + the half of the plane(lenth) - the half of the shoot
            //cordination the shoot y=the top of the plane - the half of the plane(height)
            this.x = parseInt(user.userf.style.left) + user.width / 2 - this.width / 2;
            this.y = parseInt(user.userf.style.top) - user.height / 2;
        }
        this.shooterf.style.left = this.x + "px";
        this.shooterf.style.top = this.y + "px";
    };

    //construct the shoot move
    //check the cordination of the shoot;
    this.ShooterMove = function (index, array) { // the index and arry is created to store
        this.y -= 2; //
        if (this.y <= 0) { //when the bullet is out of the scree, remove the object
            this.shooterf.remove();
            array.splice(index, 1); //delete it from the array
        }
        this.creatShooter(); //continue to create the attact
    };


    //create the bullet and define how to attact the enemy and remove from the bgmap
    this.ShooterEnemy = function (enemyarr, index, shootarr) { // the parameters are the enemy array, the location and shoot array)
        for (var key in enemyarr) { //check the enemy array
            // define whether the shoot is hitting the enemy (x and y should with in the enemy's x and y) if hit then remove 
            if (this.x > enemyarr[key].x - this.width && this.x < enemyarr[key].x + enemyarr[key].width &&
                this.y > enemyarr[key].y - this.height && this.y < enemyarr[key].y + enemyarr[key].height) {

                enemyarr[key].blood -= 1;
                if (enemyarr[key].blood <= 0) {
                    enemyarr[key].enemyf.remove();
                    enemyarr.splice(key, 1);
                }

                //同时移除击打中敌机的子弹及实例化对象
                this.shooterf.remove();
                shootarr.splice(index, 1);
            }
        }
    }
}



function Enemy(width, height, blood, score, s) {
    //there are two kind of normal enemy
    this.width = width || 40;
    this.height = height || 30;
    this.blood = blood || 3;
    this.score = score || 100;
    this.enemyf = null;
    this.src = s || "./static/enemy_image_1.png";
    this.speed = 2;
    this.count = 0;
    this.position = "absolute";
    this.x; //x left
    this.y; //y top
    //
    this.createEnemy = function () {
        if (this.enemyf == null) {
            this.enemyf = document.createElement("img");
            this.enemyf.style.width = this.width + "px";
            this.enemyf.style.height = this.height + "px";
            this.enemyf.style.position = this.position;
            this.enemyf.style.zIndex = 2;
            this.enemyf.src = this.src;
            map.appendChild(this.enemyf);
            //create the random location of the enemy x 
            this.x = Math.random() * (400 - this.width);
            this.y = -this.height;
        }
        this.enemyf.style.left = this.x + "px";
        this.enemyf.style.top = this.y + "px";
    }

    //movement of the enemy
    this.EnemyMove = function (index, array) {
        //change the y value if larger then the background then remove
        this.y += this.speed;
        if (this.y > 730) {
            this.enemyf.remove();
            array.splice(index, 1);
            score++;
        }
        this.enemyf.style.top = this.y + "px";

        //if the user has been hit by the plane than game over
        if (user.x > this.x - user.width + 20 && user.x < this.x + this.width - 20 &&
            user.y > this.y - user.height + 20 && user.y < this.y + this.height - 20) {
            total_live -= 1;
        }

        if (total_live < 1) {
            alert("Game over");
            clearInterval(time_creatShooter);
            clearInterval(time_ShooterMove);
            clearInterval(time_createEnemy);
            clearInterval(time_EnemyMove);
            map.onmousemove = null;
            return;
        }

    }
}


window.onload = function () {

    //instanlized the user
    user = new User();
    user.createUser();
    //push the shoot 
    time_creatShooter = setInterval(function () {
        shooter = new Shoot();
        shooter.creatShooter(user);
        shoot.push(shooter);
    }, 200);

    //let the bullet move
    time_ShooterMove = setInterval(function () {
        if (shoot.length > 0) {
            for (var i = 0; i < shoot.length; i++) {
                shoot[i].ShooterMove(i, shoot);
                if (em_array.length > 0) { //attact 
                    if (shoot[i] == undefined) return; //
                    shoot[i].ShooterEnemy(em_array, i, shoot);
                }
            }
        }
    }, 5);
    
    time_createEnemy = setInterval(function () {
        
        if (Math.random() < 0.7) {
            enemy = new Enemy();
            enemy.createEnemy();
            em_array.push(enemy);

        } else if (Math.random() < 0.4) {
            enemy = new Enemy(50, 65, 4, 300, "./static/enemy_image_2.png");
            enemy.createEnemy();
            em_array.push(enemy);

        } else {
            enemy = new Enemy(70, 85, 6, 500, "./static/boss_1.png");
            enemy.createEnemy();
            em_array.push(enemy);
        }
    }, 1000);
    //make the enemy move
    time_EnemyMove = setInterval(function () {
        if (em_array.length > 0) {
            for (var key in em_array) {
                em_array[key].EnemyMove(key, em_array);
            }
        }
    }, 15)
    //event linkto the mouse 
    map.onmousemove = function (e) {
        var x = e.pageX - this.offsetLeft - user.width / 2;
        var y = e.pageY - this.offsetTop - user.height / 2;
        user.UserMove(x, y);
    }
}